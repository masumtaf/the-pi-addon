<?php
namespace ThePIAddon;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Class The_PI_Addon_Manager
 *
 * Main Plugin class
 * @since 1.2.0
 */
class The_PI_Addon_Manager {

	/**
	 * Instance
	 *
	 * @since 1.2.0
	 * @access private
	 * @static
	 *
	 * @var Plugin The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.2.0
	 * @access public
	 *
	 * @return Plugin An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	/**
	 * Loading required scripts
	 * @param
	 * @return void
	 * @since 1.0.1
	 */	
	public function the_pi_enqueue_scripts() {

        wp_enqueue_style( 'bootstrap-grid.min.css', THEPIADDON_URL . 'assets/vendor/bootstrap-grid.min.css' );
        wp_enqueue_style( 'swiper', THEPIADDON_URL . 'assets/vendor/swiper.min.css' );
        wp_enqueue_style( 'the-pi-addon-css', THEPIADDON_URL . 'assets/css/the-pi-addon.css' );
		//wp_enqueue_style( 'cake-admin-css', THEPIADDON_URL . 'assets/css/the-pi-addon-admin.css' );

		wp_enqueue_script( 'the-pi-swiper', THEPIADDON_URL . 'assets/vendor/swiper.min.js', array( 'jquery'), '1.0.0', true );
		wp_enqueue_script( 'the-pi-addon-js', THEPIADDON_URL . 'assets/js/the-pi-addon.js', array( 'jquery'), '1.0.0', true );
		wp_localize_script( 'the-pi-addon-js', 'piaddon_frontend_ajax_object',
		array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' )
		) 
	);
	}

	/**
	 * Initialize the plugin widgets
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function the_pi_load_shortcode() {

	// Require shortcode Widget form shortcode
	//require THEPIADDON_FUNCTION_PATH . 'helpers.php';
	require THEPIADDON_CLASS_PATH . 'category-featured-images.php';

	// Require shortcode Widget form shortcode
	require THEPIADDON_SHORTCODE_PATH . 'shortcode-category-grid.php';
	require THEPIADDON_SHORTCODE_PATH . 'shortcode-post-side-thumb.php';
	require THEPIADDON_SHORTCODE_PATH . 'shortcode-post-carousel.php';
	require THEPIADDON_SHORTCODE_PATH . 'shortcode-post-grid.php';

	}

	public function add_image_sizes() {
		add_image_size( 'tpia-grid-size', 350, 223, true );
	}
	/**
	 *  Plugin class constructor
	 *
	 * Register plugin action hooks and filters
	 *
	 * @since 1.2.0
	 * @access public
	 */
	public function __construct() {

		// Register  style
		add_action( 'wp_enqueue_scripts', [ $this, 'the_pi_enqueue_scripts' ] );

		// Register Class
		add_action( 'init', [ $this, 'the_pi_load_shortcode' ] );
		add_action( 'init', [ $this, 'add_image_sizes' ] );
	}
}

// Instantiate Plugin Class
The_PI_Addon_Manager::instance();
