Any Post Grid View
==================

Shortcode For Grid View
-----------------------
Args :
1. Column : Bootstrap column number
2. post per page: as given
3. post type: As Given
4. related post : true / false

 * Category view: Yes
 * Related post view: Yes       [tpia-post-side-thumb related_post="0"]
 * Post Per Page: Yes           [tpia-post-side-thumb post_per_page="-1"]
 * Order: Yes                   [tpia-post-side-thumb order="ASC"]
 * Date Support: Yes            [tpia-post-side-thumb date=""]
 * Before Date Support: Yes     [tpia-post-side-thumb date_query_before="2021-01-01"]
 * After Date Support: Yes      [tpia-post-side-thumb date_query_after="2020-01-01"]
 * Compaire: Yes                [tpia-post-side-thumb date_query_compare=""]
 * Category View: Yes           [tpia-post-side-thumb category="category"]
 * Taxonomy View: Yes           [tpia-post-side-thumb taxonomy="category"]
 * Terms View: Yes              [tpia-post-side-thumb terms=""]
 * Column Support: Yes          [tpia-post-side-thumb column="3"]
 * Show Post By Post ID: Yes    [tpia-related post__in="(int)"]

echo do_shortcode( '[tpia-related]' );
echo do_shortcode( '[tpia-related post_type="post" related_post="yes" posts_per_page="9" column="3"]' );


Displaying Your Images in Your Theme
------------------------------------

There are a few filters that you can use in your theme to display the image associations created by this plugin. Please read below for detailed information.


Display a single image representing the term archive