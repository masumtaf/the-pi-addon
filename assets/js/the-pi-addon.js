jQuery(document).ready(function ($) {
	"use strict";
  

    var settings  = jQuery('.post-grid-carousel').data('swiper');
    var swiper = new Swiper(".post-grid-carousel", settings);

    var side_thumb_post_swiper = new Swiper('.tpia-post-side-container' , {
        spaceBetween: 45,
        centeredSlides: false,
        autoplay: false,
        delay: 2500,
        loop: false,
        slidesPerView: 3,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el		: ".swiper-pagination",
            clickable :true,
        },
    });

var carousel_cat_swiper = new Swiper('.tpia-cat-carousel-container' , {
        spaceBetween: 15,
        centeredSlides: false,
        autoplay: true,
        delay: 2500,
        loop: true,
        slidesPerView: 4,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el		: ".swiper-pagination",
            clickable :true,
        },
    });


});

