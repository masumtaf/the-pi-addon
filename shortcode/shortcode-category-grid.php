<?php
use ThePIAddon\ThePIAddonManager;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Sanitize Multiple HTML class
 * 
 * @package Post Category Image With Grid and Slider
 * @since 1.3
 */
function the_Piaddon_sanitize_html_classes($classes, $sep = " ") {
	$return = "";

	if( ! is_array($classes) ) {
		$classes = explode($sep, $classes);
	}

	if( ! empty( $classes ) ) {
		foreach($classes as $class){
			$return .= sanitize_html_class($class) . " ";
		}
		$return = trim( $return );
	}

	return $return;
}

function the_Piaddon_category_grid_shortcode( $atts = array() , $content ) { 

    global $post;
    $defaults = array();
    $conditional_args = array();
    // Shortcode Parameter
    $atts = extract(shortcode_atts(array(
        'size'    			=> 'full',
        'term_id' 			=> null, 
        'taxonomy'          => 'category',
        'orderby'    		=> 'name',
        'order'      		=> 'ASC',
        'show_title' 		=> 'true',
        'show_count'		=> 'true',
        'show_desc'  		=> 'true',
        'hide_empty' 		=> 'true',
        'exclude_cat'		=> array(),
        'extra_class'		=> '',
        'className'			=> '',
        'align'				=> '',
        'column'		    => 3,
        'show_bg'           => '0',
        'carousel'          => '0',
        
    ), $atts ) );

	$size 				 = ! empty( $size ) 				? $size 						: 'full';
	$term_id 	 		 = ! empty( $term_id ) 				? explode( ',', $term_id ) 		: '';
	$taxonomy 	 		 = ! empty( $taxonomy ) 			? $taxonomy 			   		: 'category';
    $show_title	 	     = ( $show_title == 'true' ) 				? true 							: false;
	$show_count	 	     = ( $show_count == 'true' ) 				? true 							: false;
	$exclude_cat 		 = ! empty( $exclude_cat )			? explode( ',', $exclude_cat )	: array();
	$show_count	 		 = ( $show_count == 'true' ) 		? true 							: false;
	$show_desc			 = ( $show_desc == 'true' ) 		? true 							: false;
	$hide_empty  		 = ( $hide_empty == 'false' ) 		? false 						: true;
	$align				 = ! empty( $align )					? 'align'.$align				: '';
	$extra_class		 = $extra_class .' '. $align .' '. $className;
	$extra_class		 = the_Piaddon_sanitize_html_classes( $extra_class );
    $count 		         = 0;
    $Gridcolumn          = ! empty( $column ) ? $column 	: 3;
    $carousel            = ! empty( $carousel ) ? $carousel : 0;
    $show_bg             = ! empty( $show_bg ) ? $show_bg 	: 0;
	// get terms and workaround WP bug with parents/pad counts
	$args = array(
        'orderby'    => $orderby,
        'order'      => $order,
        'include'    => $term_id,
        'hide_empty' => $hide_empty,
        'exclude'	 => $exclude_cat,
    );

    ob_start();
//print_r($carousel);
    $post_categories = get_terms( $taxonomy, $args );
            if ( $post_categories ) { ?>
                <div class="">
                <?php if ( '1' == $carousel ) { ?>
                <div class="tpai-category-content-wrapper">
                    <div class="swiper-container tpia-cat-carousel-container">
                        <div class="swiper-wrapper">
                <?php }else{ ?>
                    <div class="row">
                <?php } ;?>

                    <?php
                    foreach ( $post_categories as $category ) {
                        $images = get_option( 'tpai_featured_images' );
                        if( isset( $images[$category->term_id] ) ) {
                            $category_image = wp_get_attachment_image_src( $images[$category->term_id], $size );
            
                        }
                        $term_link  = get_term_link( $category, $taxonomy );
                    ?>

                    <?php if ( '1' == $carousel ) { ?>
                        <div class="swiper-slide">
                    <?php } else{ ?>
                    <div class="pb-5 col-md-<?php echo $Gridcolumn; ?>">
                    <?php } ?>
                            <div class="tpai-taxonomy-box" <?php if ( '1' == $show_bg) : ?> style="background: url('<?php echo esc_url ( $category_image['0']) ;?>');" <?php endif;?> >
                                <div class="tpai-img-wrapper">
                                    <?php if( $show_count ) { ?>
                                            <span class="tpia-cat-count"><?php echo $category->count; ?></span>
                                        <?php } ?>
                                <?php if( isset( $images[$category->term_id] ) && '0' == $show_bg ) { ?>
                                        <a href="<?php echo $term_link; ?>"><img class="tpai-cat-img" alt="<?php echo $category->name; ?>" src="<?php echo $category_image[0]; ?>"/></a>
                                <?php } ;?>
                                </div>
                                <div class="tpai-cat-content">
                                    <div class="tpai-cat-title tpia-post-title ">
                                        <?php if( $show_title && $category->name ) { ?>
                                            <a href="<?php echo $term_link; ?>"><?php echo $category->name; ?> </a>
                                        <?php } ;?>
                                        
                                    </div>
                                    <?php if( $show_desc && $category->description ) { ?>
                                        <div class="tpai-description">
                                            <div class="tpai-cat-desc"><?php echo $category->description; ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php }; wp_reset_postdata(); ?>
                </div>
                <?php if ( '1' == $carousel ) { ?>
                </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div> 
                </div>
            </div>
        <?php }else{ ?>
        </div>
        <?php } ;?>
            <?php
        }

    $content .= ob_get_clean();
	return $content;
}

add_shortcode('tpia-category-grid', 'the_Piaddon_category_grid_shortcode');